package com.alasrapp.quran.adapter;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alasrapp.quran.R;
import com.alasrapp.quran.utility.ComplexSharedPreference;
import com.alasrapp.quran.utility.StyleText;

/**
 * Created by crikle on 1/20/2016.
 */
public class ReadingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Cursor cursor;
    private Activity activity;
    private StyleText styleText;
    private Typeface typeface_title;
    private ComplexSharedPreference prefs;


    public ReadingAdapter(Activity activity, Cursor cursor) {
        this.activity = activity;
        this.cursor = cursor;

        styleText = new StyleText(activity);
        prefs = new ComplexSharedPreference(activity);

        typeface_title = Typeface.createFromAsset(activity.getAssets(), "fonts/Gotham-Black.otf");

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(activity).inflate(R.layout.row_reading, parent, false);
        TextView primary = (TextView) row.findViewById(R.id.primary);
        TextView translation = (TextView) row.findViewById(R.id.translation);
        return new VHItem(row, primary, translation);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (cursor == null) return;

        cursor.moveToPosition(position);

        //cast holder to VHItem and set data
        VHItem viewHolder = (VHItem) holder;
        viewHolder.primary.setText(cursor.getString(0));
        viewHolder.primary = styleText.styleAyah(position, viewHolder.primary, true);

        if (!prefs.getString("translation", "english").equals("None")) {
            viewHolder.translation.setVisibility(View.VISIBLE);
            viewHolder.translation.setText(cursor.getString(1));
            viewHolder.translation = styleText.styleAyah(position, viewHolder.translation, false);
        }
    }


    @Override
    public int getItemCount() {
        return cursor.getCount();
    }


    class VHItem extends RecyclerView.ViewHolder {
        TextView primary, translation;

        public VHItem(View itemView, TextView primary, TextView translation) {
            super(itemView);
            this.primary = primary;
            this.translation = translation;
        }
    }

}  //cls listView adapter
