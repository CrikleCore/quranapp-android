package com.alasrapp.quran.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.alasrapp.quran.R
import com.alasrapp.quran.pojo.Surah
import com.alasrapp.quran.utility.ComplexSharedPreference
import com.alasrapp.quran.utility.SimpleDividerItemDecoration
import com.alasrapp.quran.utility.Utility
import org.json.JSONArray
import org.json.JSONException
import java.util.*

/**
 * Created by crikle on 3/26/2015.
 */

class SurrahListActivity : AppCompatActivity() {


    lateinit var list: RecyclerView
    lateinit var array_surah: ArrayList<Surah>
    //DatabaseCreationService databaseCreationService;
    lateinit var adapter: CustomAdapter
    lateinit var typeface_text: Typeface
    internal var isTablet: Boolean = false
    internal var title_discription: Array<String?>? = null
    internal var restore_session_list = false
    lateinit var utility: Utility

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_surrah)
        typeface_text = Typeface.createFromAsset(assets, "fonts/Breve Text Light.otf")
        isTablet = resources.getBoolean(R.bool.isTablet)

        title = "Surahs"
        utility = Utility()


        title_discription = arrayOfNulls<String>(114)

        createSurahsFromJson()

        list = findViewById(R.id.List_Surrah) as RecyclerView

        val mLayoutManager = LinearLayoutManager(this)
        list.layoutManager = mLayoutManager
        list.addItemDecoration(SimpleDividerItemDecoration(
                applicationContext
        ))
        restore_session_list = false
        adapter = CustomAdapter(array_surah)
        list.adapter = adapter
    }//cls on create

    override fun onResume() {
        super.onResume()
        title = "Surrahs"
        restore_session_list = false
        createSurahsFromJson()
        adapter = CustomAdapter(array_surah)
        list.adapter = adapter

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_surrah, menu)

        return true//return true so that the menu pop up is opened

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_restore_session -> {
                //start reading activity from previous session

                val complexSharedPreference = ComplexSharedPreference(this)
                val savedSessions = complexSharedPreference.getListString("Session")

                if (savedSessions.isEmpty()) {
                    Toast.makeText(this, "No session available", Toast.LENGTH_SHORT).show()
                    return true//quick return
                }

                array_surah = ArrayList<Surah>()

                savedSessions.indices
                        .map { i -> savedSessions[i].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray() }
                        .forEach { array_surah.add(Surah(it[0], Integer.parseInt(it[1]), Integer.parseInt(it[2]))) }

                title = "Previous Sessions"
                restore_session_list = true
                adapter = CustomAdapter(array_surah)
                list.adapter = adapter
            }
        }
        return true
    }


    //Handle back from Restore session
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (restore_session_list) {
                restore_session_list = false
                createSurahsFromJson()
                adapter = CustomAdapter(array_surah)
                // Reload SurrahList instead Restore Session when back is pressed
                list.adapter = adapter
                title = "Surahs"

                return true
            }

        }//cls key code back

        return super.onKeyDown(keyCode, event)
    }


    private fun createSurahsFromJson() {
        array_surah = ArrayList<Surah>()

        try {

            val obj = JSONArray(utility.loadJSONFromAsset("Titles", this))
            val obj2 = JSONArray(utility.loadJSONFromAsset("Surahs", this))

            //initializing
            for (i in 0..113) {

                val surah_object = Surah("", 0, 0)
                val `object` = obj.getJSONObject(i + 114)

                val split = `object`.getString("title").split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                for (k in 0..split.size - 1 - 1) {

                    surah_object.title_English = surah_object.title_English + "-" + split[k]
                }
                surah_object.title_English = surah_object.title_English?.substring(1, surah_object.title_English!!.length)

                val object2 = obj2.getJSONObject(i)
                surah_object.total_Ayat = Integer.parseInt(object2.getString("total_ayah"))
                surah_object.surrah_number = i + 1
                array_surah.add(surah_object)

            }//cls for


        } catch (e: JSONException) {

            e.printStackTrace()
        }

    }//cls set title string

    class ViewHolder(itemView: View, val title: TextView, val number: TextView, val ayat: TextView, val row: LinearLayout, //used for tablet
                              val title_discription: TextView?) : RecyclerView.ViewHolder(itemView)//cls view holder

    inner class CustomAdapter(private val mDataset: ArrayList<Surah>) : RecyclerView.Adapter<SurrahListActivity.ViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SurrahListActivity.ViewHolder {
            val row = LayoutInflater.from(this@SurrahListActivity).inflate(R.layout.row_surrah, viewGroup, false)
            val title = row.findViewById(R.id.Title_English_Surrah) as TextView
            val number = row.findViewById(R.id.surrah_number) as TextView
            val ayat = row.findViewById(R.id.total_ayat) as TextView
            val table = row.findViewById(R.id.row_surrah) as LinearLayout
            val title_description: TextView? = null
            //            if (i dfdsTablet) title_description = (TextView) row.findViewById(R.id.title_discription);
            val vh = ViewHolder(row, title, number, ayat, table, title_description)

            return vh
        }

        override fun onBindViewHolder(viewHolder: SurrahListActivity.ViewHolder, i: Int) {

            if (!isTablet)
                mDataset[i].title_English = mDataset[i].title_English?.replace(" ", "")

            viewHolder.title.text = mDataset[i].title_English
            viewHolder.title.typeface = typeface_text

            viewHolder.number.text = (i + 1).toString() + ""


            //load reading activity on click
            viewHolder.row.setOnClickListener {
                //                    viewHolder.row.setBackgroundColor(Color.parseColor("#D3D3D3"));

                val intent = Intent(this@SurrahListActivity, com.alasrapp.quran.activities.ReadingActivity::class.java)
                val name = mDataset[i].title_English
                intent.putExtra("title", name)
                //total_ayat = scroll position in restore session
                if (restore_session_list)
                    intent.putExtra("scroll_position", mDataset[i].total_Ayat)
                intent.putExtra("surahId", mDataset[i].surrah_number.toString() + "")
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            //            viewHolder.row.setBackgroundColor(Color.parseColor("#F6EAD2"));

            viewHolder.ayat.text = mDataset[i].total_Ayat.toString() + ""

            //            if (isTablet) {
            //
            //                viewHolder.title_discription.setText(title_discription[i]);
            //                viewHolder.title_discription.setTypeface(typeface_text);
            //
            //            }


        }//cls if not translation nono

        override fun getItemCount(): Int {
            return mDataset.size
        }


    }  //cls listView adapter

}//cls cls
