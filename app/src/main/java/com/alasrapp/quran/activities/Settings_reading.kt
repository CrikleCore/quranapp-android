package com.alasrapp.quran.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.widget.RadioGroup
import android.widget.Switch
import com.alasrapp.quran.R
import com.alasrapp.quran.utility.ChangeLanguage
import com.alasrapp.quran.utility.ComplexSharedPreference

class Settings_reading : AppCompatActivity() {
    private var prefs: ComplexSharedPreference? = null
    private var fontSize: Int = 0
    private var ayahNumber: Switch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_reading_activity3)
        prefs = ComplexSharedPreference(this)
        val cl = ChangeLanguage(this)
        cl.handle_change_language()
        handleChangeFont()
        ayahNumber = findViewById(R.id.ayah_number)
        ayahNumber!!.isChecked = prefs!!.getBoolean("ShowAyahNumber", true)
        ayahNumber!!.setOnCheckedChangeListener { _, isChecked -> prefs!!.putBoolean("ShowAyahNumber", isChecked) }
    }

    private fun handleChangeFont() {
        fontSize = prefs!!.getInt("fontSize", resources.getDimension(R.dimen.normal).toInt())

        val group = findViewById<RadioGroup>(R.id.radio_group)
        group!!.clearCheck()
        if (fontSize == resources.getDimension(R.dimen.small).toInt())
            group.check(R.id.small_button)
        if (fontSize == resources.getDimension(R.dimen.normal).toInt())
            group.check(R.id.normal_button)
        if (fontSize == resources.getDimension(R.dimen.large).toInt())
            group.check(R.id.large_button)
        if (fontSize == resources.getDimension(R.dimen.huge).toInt())
            group.check(R.id.huge_button)

        group.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.small_button -> fontSize = resources.getDimension(R.dimen.small).toInt()
                R.id.normal_button -> fontSize = resources.getDimension(R.dimen.normal).toInt()
                R.id.large_button -> fontSize = resources.getDimension(R.dimen.large).toInt()
                R.id.huge_button -> fontSize = resources.getDimension(R.dimen.huge).toInt()
            }
        }


    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            prefs!!.putInt("fontSize", fontSize)
            val array = prefs!!.getListString("Session")
            val session = array[0].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            val intent = Intent(this, ReadingActivity::class.java)

            intent.putExtra("title", session[0])
            //total_ayat = scroll position in restore session
            intent.putExtra("scroll_position", Integer.parseInt(session[2]))
            intent.putExtra("surahId", session[1])
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            return true
        } else {
            return super.onKeyDown(keyCode, event)
        }
    }

    override fun onDestroy() {
        prefs!!.putInt("fontSize", fontSize)
        super.onDestroy()
    }
}
