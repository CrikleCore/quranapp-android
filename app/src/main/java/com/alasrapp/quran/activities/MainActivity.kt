package com.alasrapp.quran.activities

import android.os.Build
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.MotionEvent
import android.view.WindowManager
import com.alasrapp.quran.R


class MainActivity : AppCompatActivity(), SurahListFragment.SurahInteractionListener {

    //Toolbar toolbar;
    private lateinit var mViewPager: ViewPager
    private lateinit var mAdapter: SectionPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        //Change actionbar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
            Build.VERSION.CODENAME
        }

        mViewPager = findViewById(R.id.container) as ViewPager
        mAdapter = SectionPagerAdapter(supportFragmentManager)
        mViewPager.adapter = mAdapter
        val tab = findViewById(R.id.tabs) as TabLayout?

        tab!!.setupWithViewPager(mViewPager)

    }


    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        super.dispatchTouchEvent(ev)
        return mViewPager.onTouchEvent(ev)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.action_settings)
            return true

        return super.onOptionsItemSelected(item)
    }


    override fun onSurahSelected(surahId: Int) {

    }

    internal inner class SectionPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            val fragment = SurahListFragment()
            val bundle = Bundle()

            if (position == 0)
                bundle.putBoolean("recentFragment", false)
            else
                bundle.putBoolean("recentFragment", true)

            fragment.arguments = bundle
            return fragment


        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0)
                return "Surahs"
            return "Recent"
        }
    }
}
