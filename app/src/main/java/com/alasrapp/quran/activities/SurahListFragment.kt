package com.alasrapp.quran.activities

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.alasrapp.quran.R
import com.alasrapp.quran.activities.SurahListFragment.SurahInteractionListener
import com.alasrapp.quran.pojo.Surah
import com.alasrapp.quran.utility.ComplexSharedPreference
import com.alasrapp.quran.utility.SimpleDividerItemDecoration
import com.alasrapp.quran.utility.Utility
import org.json.JSONArray
import org.json.JSONException
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SurahInteractionListener] interface
 * to handle interaction events.
 * Use the [SurahListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SurahListFragment : Fragment() {

    private var mListener: SurahInteractionListener? = null


    lateinit var mRecylerView: RecyclerView
    lateinit var array_surah: ArrayList<Surah>
    //DatabaseCreationService databaseCreationService;
    lateinit var adapter: CustomAdapter
    lateinit var typeface_text: Typeface
    internal var isTablet: Boolean = false
    internal var title_discription: Array<String?>? = null
    internal var recentFragment: Boolean = false
    lateinit var utility: Utility


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_surrah, container, false)
        typeface_text = Typeface.createFromAsset(activity?.assets, "fonts/Breve Text Light.otf")
        isTablet = resources.getBoolean(R.bool.isTablet)

        //        setTitle("Surahs");
        utility = Utility()
        val bundle = arguments
        recentFragment = bundle?.getBoolean("recentFragment") ?: false

        title_discription = arrayOfNulls(114)


        populateAdapterArray()

        mRecylerView = view.findViewById(R.id.List_Surrah) as RecyclerView

        val mLayoutManager = LinearLayoutManager(activity)
        mRecylerView.layoutManager = mLayoutManager
        mRecylerView.addItemDecoration(SimpleDividerItemDecoration(
            activity?.applicationContext
        ))
        adapter = CustomAdapter(array_surah)
        mRecylerView.adapter = adapter
        return view
    }

    override fun onResume() {
        //messy way to refresh recent
        if (recentFragment) {
            populateAdapterArray()
            adapter = CustomAdapter(array_surah)
            mRecylerView.adapter = adapter
        }

        super.onResume()
    }

    private fun populateAdapterArray() {
        array_surah = ArrayList<Surah>()

        if (!recentFragment) {

            try {

                val obj = JSONArray(utility.loadJSONFromAsset("Titles", activity))
                val obj2 = JSONArray(utility.loadJSONFromAsset("Surahs", activity))

                //initializing
                for (i in 0..113) {

                    val surah_object = Surah("", 0, 0)
                    val `object` = obj.getJSONObject(i + 114)

                    val split = `object`.getString("title").split("-".toRegex())
                        .dropLastWhile { it.isEmpty() }.toTypedArray()
                    for (k in 0..split.size - 1 - 1) {

                        surah_object.title_English = surah_object.title_English + "-" + split[k]
                    }
                    surah_object.title_English = surah_object.title_English?.substring(1,
                        surah_object.title_English!!.length)

                    val object2 = obj2.getJSONObject(i)
                    surah_object.total_Ayat = Integer.parseInt(object2.getString("total_ayah"))
                    surah_object.surrah_number = i + 1
                    array_surah.add(surah_object)

                }//cls for


            } catch (e: JSONException) {

                e.printStackTrace()
            }

        } else {
            val complexSharedPreference = ComplexSharedPreference(context)
            val savedSessions = complexSharedPreference.getListString("Session")
            for (i in savedSessions.indices) {
                val split = savedSessions[i].split(":".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
                array_surah.add(Surah(split[0],
                    Integer.parseInt(split[1]),
                    Integer.parseInt(split[2])))
            }

        }

    }


    fun onSurahSelected(surahId: Int) {
        if (mListener != null) {
            mListener!!.onSurahSelected(surahId)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is SurahInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement SurahInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface SurahInteractionListener {
        fun onSurahSelected(surahId: Int)
    }


    inner class CustomAdapter(private val mDataset: ArrayList<Surah>) :
        RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
            val row = LayoutInflater.from(activity).inflate(R.layout.row_surrah, viewGroup, false)
            val title = row.findViewById(R.id.Title_English_Surrah) as TextView
            val number = row.findViewById(R.id.surrah_number) as TextView
            val ayat = row.findViewById(R.id.total_ayat) as TextView
            val text_total_ayah = row.findViewById(R.id.text_total_ayat) as TextView
            val table = row.findViewById(R.id.row_surrah) as LinearLayout
            val title_description: TextView? = null
            //            if (isTablet) title_description = (TextView) row.findViewById(R.id.title_discription);
            val vh = ViewHolder(row, title, number, ayat, table, title_description, text_total_ayah)
            return vh
        }


        override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

            if (!isTablet)
                mDataset[i].title_English = mDataset[i].title_English?.replace(" ", "")

            viewHolder.title.text = mDataset[i].title_English
            if (recentFragment)
                viewHolder.text_total_ayat.text = "ayah"
            else
                viewHolder.text_total_ayat.text = "ayahs"

            viewHolder.title.typeface = typeface_text

            viewHolder.number.text = (i + 1).toString() + ""


            //load reading activity on click
            viewHolder.row.setOnClickListener {
                //                    viewHolder.row.setBackgroundColor(Color.parseColor("#D3D3D3"));

                val intent =
                    Intent(activity, com.alasrapp.quran.activities.ReadingActivity::class.java)
                val name = mDataset[i].title_English
                intent.putExtra("title", name)
                //total_ayat = scroll position in restore session
                if (recentFragment)
                    intent.putExtra("scroll_position", mDataset[i].total_Ayat)
                intent.putExtra("surahId", mDataset[i].surrah_number.toString() + "")
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivityForResult(intent, 1)
            }

            //            viewHolder.row.setBackgroundColor(Color.parseColor("#F6EAD2"));

            viewHolder.ayat.text = mDataset[i].total_Ayat.toString() + ""

            //            if (isTablet) {
            //
            //                viewHolder.title_discription.setText(title_discription[i]);
            //                viewHolder.title_discription.setTypeface(typeface_text);
            //
            //            }


        }

        override fun getItemCount(): Int {
            return mDataset.size
        }


        inner class ViewHolder(itemView: View,
                               internal val title: TextView,
                               internal val number: TextView,
                               internal val ayat: TextView,
                               internal val row: LinearLayout, //used for tablet
                               internal val title_discription: TextView?,
                               internal val text_total_ayat: TextView) :
            RecyclerView.ViewHolder(itemView)

    }

    companion object {

        /**
         * Use this factory method to create a new instance of this fragment.

         * @return A new instance of fragment SurahListFragment.
         */
        fun newInstance(): SurahListFragment {
            return SurahListFragment()
        }
    }

}// Required empty public constructor
