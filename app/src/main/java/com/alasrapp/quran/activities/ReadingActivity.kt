package com.alasrapp.quran.activities

import android.content.Intent
import android.database.Cursor
import android.graphics.Point
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import com.alasrapp.quran.R
import com.alasrapp.quran.adapter.ReadingAdapter
import com.alasrapp.quran.utility.ComplexSharedPreference
import com.alasrapp.quran.utility.DbHelper
import com.alasrapp.quran.utility.GOTO
import java.util.*


class ReadingActivity : AppCompatActivity() {

    internal lateinit var ayatList: RecyclerView
    internal lateinit var cursor: Cursor
    internal lateinit var adapter: ReadingAdapter
    internal lateinit var mLayoutManager: LinearLayoutManager
    internal lateinit var prefs: ComplexSharedPreference
    internal lateinit var title: String
    internal lateinit var surahId: String
    internal lateinit var recentSessions: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reading)
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        initializeVariables()
        ayatList = findViewById(R.id.AyatLayout) as RecyclerView
        mLayoutManager = LinearLayoutManager(this@ReadingActivity)
        ayatList.layoutManager = mLayoutManager

    }


    private fun initializeVariables() {
        prefs = ComplexSharedPreference(this)
        title = intent.extras.getString("title")
        surahId = intent.extras.getString("surahId")
        supportActionBar?.title = title
        recentSessions = prefs.getListString("Session")
        //First time launch
        LoadDataFromDB().execute("")

    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        saveReadingSession()
        return super.onKeyDown(keyCode, event)
    }


     inner class LoadDataFromDB : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg params: String): String? {
            val db = DbHelper(this@ReadingActivity)
            cursor = db.getAyat(prefs, surahId)
            return null
        }


        override fun onPostExecute(o: String?) {

            adapter = ReadingAdapter(this@ReadingActivity, cursor)
            ayatList.adapter = adapter

            //scroll if restore previous session is called
            val scroll = intent.extras.getInt("scroll_position", -1)
            if (scroll != -1) {
                deleteSession(scroll)
                mLayoutManager.scrollToPosition(scroll - 1)
            }


        }
    }


    override fun onStop() {
        super.onStop()
        saveReadingSession()
    }

    override fun onResume() {
        super.onResume()
        //TODO: check if it works without problems without this if condition
//        if (mLayoutManager != null)
        deleteSession(mLayoutManager.findFirstVisibleItemPosition())
    }

    private fun saveReadingSession() {
        var position = mLayoutManager.findFirstVisibleItemPosition()
        position++
        val text = "$title:$surahId:$position"

        if (recentSessions.size > 0) {
            if (recentSessions[0] == text)
                return
        }
        recentSessions.add(0, text)

        if (recentSessions.size > 5)
            recentSessions.removeAt(5)

        prefs.putListString("Session", recentSessions)

    }

    private fun deleteSession(scrollPosition: Int) {
        val text = "$title:$surahId:$scrollPosition"
        recentSessions.remove(text)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_reading, menu)
        return true // return true so that the menu pop up is opened
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.GOTO -> {

                val goto_ = GOTO()
                var alert: AlertDialog? = null
                alert = goto_.showDialog(this, cursor, alert)
                alert.setOnCancelListener {
                    if (GOTO.pos != -1 && GOTO.pos != 0)
                        mLayoutManager.scrollToPosition(GOTO.pos - 1)
                }
            }

            R.id.settings_reading -> {
                //         fullScreenMode = false;
                val intent = Intent(this, Settings_reading::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }


        return true
    }

}
