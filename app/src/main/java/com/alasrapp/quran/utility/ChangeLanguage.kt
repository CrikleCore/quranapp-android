package com.alasrapp.quran.utility

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.alasrapp.quran.R
import java.util.*

/**
 * Created by waahm on 8/3/2015.
 */
class ChangeLanguage(private val activity: Activity) {

    private val prefs: com.alasrapp.quran.utility.ComplexSharedPreference
    private var primary: String? = null
    private var translation: String? = null
    private val primarySpin: Spinner
    private val translationSpin: Spinner
    private var list: ArrayList<String>? = null

    init {
        prefs = com.alasrapp.quran.utility.ComplexSharedPreference(activity)

        primary = prefs.getString("primary", "Arabic")
        translation = prefs.getString("translation", "English")
        primarySpin = activity.findViewById(R.id.primaryLanguage) as Spinner
        translationSpin = activity.findViewById(R.id.translation) as Spinner

    }//cls constructor

    fun handle_change_language() {
        addLanguages()
        setSpin()
    }//cls handle_change_language


    private fun setSpin() {

        val adapter1 = CustomAdapter(activity, R.layout.spinner_layout, list, true)

        val adapter2 = CustomAdapter(activity, R.layout.spinner_layout, list, false)

        //        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
        //                android.R.layout.simple_spinner_item,
        //                list) {
        //            @Override
        //            public View getView(int position, View convertView, ViewGroup parent) {
        //
        //                View v = super.getView(position, convertView, parent);
        //                ((TextView) v).setTextColor(Color.parseColor("#000000"));
        //                return v;
        //            }
        //
        //            @Override
        //            public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //                View v = super.getDropDownView(position, convertView, parent);
        //
        //                ((TextView) v).setTextColor(Color.BLACK);
        //
        //
        //                return v;
        //            }
        //        };
        adapter1.setDropDownViewResource(android.R.layout.simple_list_item_single_choice)

        adapter2.setDropDownViewResource(android.R.layout.simple_list_item_single_choice)

        primarySpin.adapter = adapter1
        primarySpin.setSelection(list!!.indexOf(primary))

        translationSpin.adapter = adapter2
        translationSpin.setSelection(list!!.indexOf(translation))


        primarySpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                if (list!![position] == "None") {
                    Toast.makeText(activity, "You can't select none for primary language", Toast.LENGTH_SHORT).show()
                    primarySpin.setSelection(list!!.indexOf(primary))
                    return
                }

                primary = list!![position]
                if (list!![position] == translation) {
                    Toast.makeText(activity, "Primary and tranlation can't be same.", Toast.LENGTH_SHORT).show()
                    translation = "None"
                    translationSpin.setSelection(0)
                    prefs.putString("translation", translation)

                }

                prefs.putString("primary", primary)
                if (selected_primary != null)
                    selected_primary!!.text = primary

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        translationSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (list!![position] != primary)
                    translation = list!![position]
                else {

                    Toast.makeText(activity, "Primary and tranlation can't be same.", Toast.LENGTH_SHORT).show()
                    translation = "None"
                }


                prefs.putString("translation", translation)
                translationSpin.setSelection(list!!.indexOf(translation!!))
                if (selected_translation != null)
                    selected_translation!!.text = translation
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }


    internal var selected_primary: TextView? = null
    internal var selected_translation: TextView? = null

    private inner class CustomAdapter(context: Context, resource: Int, objects: ArrayList<String>?, internal var mainText: Boolean) : ArrayAdapter<String>(context, resource, objects) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val v = inflater.inflate(R.layout.spinner_layout, parent, false)
            val title = v.findViewById(R.id.title_spinner) as TextView
            title.setTextColor(Color.BLACK)
            if (mainText) {
                title.text = "Main Text"
                selected_primary = v.findViewById(R.id.selected_spinner) as TextView
                selected_primary!!.text = primary
                selected_primary!!.setTextColor(Color.BLACK)
            } else {
                title.text = "Translation"
                selected_translation = v.findViewById(R.id.selected_spinner) as TextView
                selected_translation!!.text = translation
                selected_translation!!.setTextColor(Color.BLACK)
            }
            return v
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val v = super.getDropDownView(position, convertView, parent)
            if (v != null)
                (v as TextView).setTextColor(Color.BLACK)
            return v
        }
    }


    private fun addLanguages() {

        list = ArrayList<String>()
        list!!.add("None")
        list!!.add("Arabic")
        list!!.add("Bengali")
        list!!.add("Bulgarian")
        list!!.add("Chinese")
        list!!.add("Czech")
        list!!.add("Dutch")
        list!!.add("English")
        list!!.add("French")
        list!!.add("German")
        list!!.add("Hindi")
        list!!.add("Indonesian")
        list!!.add("Italian")
        list!!.add("Japanese")
        list!!.add("Persian")
        list!!.add("Portuguese")
        list!!.add("Russian")
        list!!.add("Spanish")
        list!!.add("Turkish")
        list!!.add("Urdu")


    }//cls list1


}
