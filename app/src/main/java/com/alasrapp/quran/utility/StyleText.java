package com.alasrapp.quran.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SuperscriptSpan;
import android.widget.TextView;

import com.alasrapp.quran.R;

import java.util.Arrays;

/**
 * Created by waqar on 6/29/2015.
 */
public class StyleText {
    private Typeface typeface_arabic_text, typeface_urdu;
    private ComplexSharedPreference prefs;
    private ColorCodedTanween colorCodedTanween;
    private static final String[] Rtl_Languages = {"Arabic", "Azeri", "Bakhtiari", "Balochi", "Persian", "Gilaki", "Javanese3", "Kashmiri", "Kazakh3", "Kurdish", "Malay3", "Malayalam3", "Pashto", "Punjabi", "Qashqai", "Sindhi", "Somali2", "Sulu", "Takestani", "Turkmen", "Uighur", "WesternCham", "Hebrew", "Ladino", "Yiddish", "Mandekan", "Assyrian", "ModernAramaicKoine", "Syriac", "Maldivian", "Tamashek", "Urdu"};
    private Context c;

    public StyleText(Context c) {

        //  typeface_text = Typeface.createFromAsset(c.getAssets(), "fonts/Breve Text Light.otf");
        //  typeface_title = Typeface.createFromAsset(c.getAssets(), "fonts/Breve Text Bold.otf");
        typeface_arabic_text = Typeface.createFromAsset(c.getAssets(), "fonts/noorehuda.ttf");
        typeface_urdu = Typeface.createFromAsset(c.getAssets(), "fonts/Mehr-Nastaliq-Web-version-1.0-beta.ttf");
        colorCodedTanween = new ColorCodedTanween();
        prefs = new ComplexSharedPreference(c);
        this.c = c;

    }//cls constructor

    public TextView styleAyah(int position, TextView textView, boolean primary) {
        String language;
        if (primary)
            language = prefs.getString("primary", "Arabic");
        else
            language = prefs.getString("translation", "English");


        if (Arrays.asList(Rtl_Languages).contains(language))
            textView = styleRTLText(position, textView, primary, language);
        else
            textView = styleEnglishText(position, textView, primary);


        return textView;
    }//cls styleAyah


    private TextView styleEnglishText(int position, TextView textview, boolean primary) {
        String text = textview.getText() + "";
        text = text.replace("\u06db", "");

        SpannableString styleText;
        int size = prefs.getInt("fontSize", (int) c.getResources().getDimension(R.dimen.normal)) - 3;
        if (primary) {
            String AyahNumber = (position + 1) + "";
            if (!prefs.getBoolean("ShowAyahNumber", true)) AyahNumber = "*";
            styleText = new SpannableString(AyahNumber + "" + text);
            int startNumber = 0;
            int endNumber = 0;
            endNumber = String.valueOf(AyahNumber).length();
            styleText.setSpan(new SuperscriptSpan(), startNumber, endNumber, 0);
            styleText.setSpan(new RelativeSizeSpan(0.5f), startNumber, endNumber, 0);
            if (text.subSequence(0, 1).equals("[") || text.subSequence(0, 1).equals("\""))
                endNumber++;
            styleText.setSpan(new StyleSpan(Typeface.BOLD), endNumber, endNumber + 1, 0);

        } else {
            size = (int) (size * 0.7);
            styleText = new SpannableString(text);
        }


        //Handle If ayahNumber is off


        textview.setText(styleText);
        textview.setTextSize(size);

        return textview;

    }

    private TextView styleRTLText(int position, TextView textview, boolean primary, String language) {

        int size = prefs.getInt("fontSize", (int) c.getResources().getDimension(R.dimen.normal));
        String text = textview.getText() + "";

        if (language.contains("Arabic")) {
            size = (int) (size * 1.3);
            textview.setTypeface(typeface_arabic_text);
            textview.setText(colorCodedTanween.ColorCodedTajweed(textview.getText() + ""));
        } else if (!primary) size = (int) (size * 0.8);

        if (language.equals("Urdu") || language.equals("Persian"))
            textview.setTypeface(typeface_urdu);

        if (text.contains(c.getResources().getString(R.string.Bismillah)))
            text = text.replace(c.getResources().getString(R.string.Bismillah), "");

        Spanned spanned_text = (Spanned) TextUtils.concat(text, new SpannableString(""));


        if (primary) {
            String AyahNumber = (position + 1) + "";
            if (!prefs.getBoolean("ShowAyahNumber", true)) AyahNumber = "-";
            if (language.contains("Arabic")) {

                //     SpannableString ss = colorCodedTanween.ColorCodedTajweed(text);
                spanned_text = (Spanned) TextUtils.concat(text, styleNumberCircle(AyahNumber, true));

            } else
                spanned_text = (Spanned) TextUtils.concat(text, styleNumberCircle(AyahNumber, true));

        }
        //Handle If ayahNumber is off


        textview.setTextSize(size);

        textview.setText(spanned_text);
        return textview;

    }//cls styleRTL


    //\u064c \u0628
    private Spanned styleNumberCircle(String position, boolean arabic) {
        float paren_size, number_size;

        if (arabic) {
            paren_size = 0.7f;
            number_size = 0.6f;
        } else {
            paren_size = 1f;
            number_size = 0.6f;
        }
        SpannableString parethesis1 = new SpannableString("(");
        SpannableString parethesis2 = new SpannableString(")");
        SpannableString number = new SpannableString(position + "");
        parethesis1.setSpan(new RelativeSizeSpan(paren_size), 0, 1, 0);
        parethesis2.setSpan(new RelativeSizeSpan(paren_size), 0, 1, 0);
        number.setSpan(new RelativeSizeSpan(number_size), 0, number.length(), 0);
        number.setSpan(new StyleSpan(Typeface.BOLD), 0, number.length(), 0);

        Spanned combined = (Spanned) TextUtils.concat(parethesis1, number, parethesis2);

        return combined;

    }//cls number circle


}
