package com.alasrapp.quran.utility;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.alasrapp.quran.R;

/**
 * Created by waqar on 7/3/2015.
 */
public class GOTO {
    public static int pos = -1;

    public AlertDialog showDialog(final Context c, final Cursor cursor, AlertDialog alertDialog) {

        LayoutInflater li = LayoutInflater.from(c);
        View promptsView = li.inflate(R.layout.goto_dialog_box, null);
        final InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                c);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.dialog_edit_text);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                //       result.setText(userInput.getText());
                                imm.hideSoftInputFromWindow(userInput.getWindowToken(), 0);

                                pos = Integer.parseInt(userInput.getText() + "");
                                pos = (pos < 0) ? -pos : pos;

                                if (pos > cursor.getCount()) {
                                    Toast.makeText(c, "Your entered ayah number exceeds total ayat.", Toast.LENGTH_SHORT).show();
                                    pos = -1;
                                }

                                dialog.cancel();

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                imm.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                                pos = -1;
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        userInput.requestFocus();
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        final AlertDialog finalAlertDialog = alertDialog;
        userInput.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    pos = Integer.parseInt(userInput.getText() + "");
                    pos = (pos < 0) ? -pos : pos;

                    if (pos > cursor.getCount()) {
                        Toast.makeText(c, "Your entered ayah number exceeds total ayat.", Toast.LENGTH_SHORT).show();
                        pos = -1;
                    }
                    imm.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                    finalAlertDialog.cancel();

                    return false;
                }


                return false;
            }

        });


        return alertDialog;

    }//cls show dialog


}
