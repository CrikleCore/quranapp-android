package com.alasrapp.quran.utility;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by waqar on 7/5/2015.
 */

public class DbHelper extends SQLiteAssetHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_Name = "ayah_text";
    public static final String Database_Name = "QuranLanguages.db";


    public DbHelper(Context context) {
        super(context, Database_Name, null, DATABASE_VERSION);
    }

    public Cursor getAyat(ComplexSharedPreference prefs, String surahId) {

        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();


        String primary = prefs.getString("primary", "Arabic");
        String translation = prefs.getString("translation", "English");

        String column[];
        //get English Ayah if language is English
        if (!translation.equals("None"))
            column = new String[]{primary, translation};
        else column = new String[]{primary};


        qb.setTables(TABLE_Name);
        Cursor cursor = qb.query(db, column, "surah_id=?", new String[]{surahId}, null,
                null, null, null);
        cursor.moveToFirst();
        return cursor;

    }
}