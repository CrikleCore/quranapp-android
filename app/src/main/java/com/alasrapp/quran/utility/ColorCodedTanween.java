package com.alasrapp.quran.utility;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

/**
 * Created by waqar on 7/4/2015.
 */
public class ColorCodedTanween {


    public SpannableString ColorCodedTajweed(String text) {

        SpannableString ayah = new SpannableString(text);


/*
        //miam three dots (orange)
//        ayah = colorOrange(ayah, letter);

        //non three dots (orange)
        letter = "\u0646\u0651";
        //      ayah = colorOrange(ayah, letter);
  */


//orange
        ayah = color_orange(ayah, "مّ");

        ayah = color_orange(ayah, "نّ");

        //green (sakan)
        ayah = color_tanween(ayah, "\u0652");

        //Red blue purple grey

        //uoon
        ayah = color_tanween(ayah, "\u064c");
        //uan
        ayah = color_tanween(ayah, "\u064b");
        //uin
        ayah = color_tanween(ayah, "\u064d");
        //non
        ayah = color_tanween(ayah, "\u0646");
        //mem
        ayah = color_tanween(ayah, "\u0645");

        return ayah;
    }//cls color coded tajweed

    private SpannableString color_green(SpannableString spanString, String keyword) {
        String text = spanString.toString();
        int index = text.indexOf(keyword);
        while (index > 0) {


            index = text.indexOf(keyword, index + keyword.length());
        }//cls while
        return spanString;
    }//cls color green


    private SpannableString color_orange(SpannableString spanString, String keyword) {
        String text = spanString.toString();
        int index = text.indexOf(keyword);
        while (index > 0) {

            int start = 1;
            int end = 2;
            while (true) {
                if (text.length() <= index + end)
                    break;
                if (isHarkaat(text.substring(index + start, index + end))) {
                    end++;
                    start++;
                } else break;

            }
            if (!isHarkaat(text.substring(index + start, index + end)))
                end--;
            spanString.setSpan(new ForegroundColorSpan(Color.parseColor("#ffa500")), index, index + end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            index = text.indexOf(keyword, index + keyword.length());

        }//cls while
        return spanString;
    }//cls orange


    private SpannableString color_tanween(SpannableString spanString, String keyword) {
        String text = spanString.toString();
        //Last letter green

        if (text.substring(text.length() - 2, text.length() - 1).equals("ب")
                || text.substring(text.length() - 2, text.length() - 1).equals("ق")
                || text.substring(text.length() - 2, text.length() - 1).equals("د")
                || text.substring(text.length() - 2, text.length() - 1).equals("ج")
                || text.substring(text.length() - 2, text.length() - 1).equals("ط"))
            spanString.setSpan(new ForegroundColorSpan(Color.GREEN), text.length() - 2, text.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);


        int index = text.indexOf(keyword);
        while (index >= 0) {
            int[] omit = calculateIndexStartEnd(text, index, keyword.length());
            int start = omit[0];
            int end = omit[1];

            if (text.length() <= index + keyword.length() + end + 1)
                break;
            //handle meem
            start = index + keyword.length() + start;
            end = index + keyword.length() + end;

            switch (keyword) {

                case "\u0652":
                    //b kaf  dal
                    if (text.substring(index - 1, index).equals("ب")
                            || text.substring(index - 1, index).equals("ق")
                            || text.substring(index - 1, index).equals("د")
                            || text.substring(index - 1, index).equals("ج")
                            || text.substring(index - 1, index).equals("ط"))
                        spanString.setSpan(new ForegroundColorSpan(Color.GREEN), index - 1, index + 1, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    break;

                case "م":
                    if (text.substring(start, end).equals("\u0628"))
                        //Color Every harakt + start word before bay
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.RED);


                    //  \u0645\u0651
                    if (text.substring(start, end + 1).equals("\u0645\u0651"))
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.parseColor("#551a8b"));

                    break;


                default:
                    //bay ( blue )
                    if (text.substring(start, end).equals("\u0628"))
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.BLUE);

                    // /ray lam (grey)
                    if (text.substring(start, end).equals("\u0631") || text.substring(start, end).equals("\u0644"))
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.LTGRAY);

                    //noon  miam w y (purple)
                    if (text.substring(start, end).equals("\u0646")
                            || text.substring(start, end).equals("\u0645")
                            || text.substring(start, end).equals("\u0648")
                            || text.substring(start, end).equals("\u064a"))
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.parseColor("#551a8b"));

                    //toi,zoi,fay,kaf,kaf,tay,say,dal,zay,dal,seen,sheen,soaad,dhoaad (red)
                    if (text.substring(start, end).equals("\u0637")
                            || text.substring(start, end).equals("\u0638")
                            || text.substring(start, end).equals("\u0641")
                            || text.substring(start, end).equals("\u0642")
                            || text.substring(start, end).equals("\u0643")
                            || text.substring(start, end).equals("\u062a")
                            || text.substring(start, end).equals("\u062b")
                            || text.substring(start, end).equals("\u062C")
                            || text.substring(start, end).equals("\u0630")
                            || text.substring(start, end).equals("\u062f")
                            || text.substring(start, end).equals("\u0632")
                            || text.substring(start, end).equals("\u0633")
                            || text.substring(start, end).equals("\u0634")
                            || text.substring(start, end).equals("\u0635")
                            || text.substring(start, end).equals("\u0636")
                            )
                        spanString = setTanweenColor(spanString, index, keyword, omit[1], Color.RED);

                    break;
            }


            index = text.indexOf(keyword, index + keyword.length());

        }
        return spanString;

    }//find index

    private int[] calculateIndexStartEnd(String text, int index, int keywordLength) {
        int start = 0;
        int end = 1;
        while (true) {
            if (text.length() <= (index + keywordLength + end + 1))
                break;
            if (text.substring(index + keywordLength + start, index + keywordLength + end).equals("\u0627")
                    || text.substring(index + keywordLength + start, index + keywordLength + end).equals(" ")
                    || text.substring(index + keywordLength + start, index + keywordLength + end).equals("\u0649")
                    || text.substring(index + keywordLength + start, index + keywordLength + end).equals("\u0648")) {

                if (!isHarkaat(text.substring(index + keywordLength + start + 1, index + keywordLength + end + 1))) {
                    start++;
                    end++;

                } else
                    break;
            } else
                break;

        }//cls true


        return new int[]{start, end};
    }//cls calculateIndexStartEnd


    private SpannableString setTanweenColor(SpannableString spanString, int index, String keyword, int end, int color) {
        String text = spanString.toString();
        //if non then don't color before index
        int start = -1;

        if (!keyword.equals("م") && !keyword.equals("ن")) {
            while (true) {

                if (isHarkaat(text.substring(index + start, index + 1 + start)))
                    start--;
                else {
                    start--;
                    break;
                }
            }//cls while
        }
        //Color Every harakat followed by bay
        while (true) {

            if (isHarkaat(text.substring(index + keyword.length() + 1 + end, index + keyword.length() + 2 + end)))
                end++;
            else
                break;

        }//cls while

        spanString.setSpan(new ForegroundColorSpan(color), index + keyword.length() + start, index + keyword.length() + end + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spanString;
    }

/*
    private SpannableString colorOrange(SpannableString spanString, String keyword) {
        String text = spanString.toString();
        int index = text.indexOf(keyword);
        while (index >= 0) {
            //Orange
            int end = 0;
            try {
                if (isHarkaat(text.substring(index + keyword.length(), keyword.length() + index + 1)))
                    end++;
            } catch (Exception e) {
                end = 0;
            }

            spanString.setSpan(new ForegroundColorSpan(Color.parseColor("#ffa500")), index, index + keyword.length() + end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = text.indexOf(keyword, index + keyword.length());

        }

        return spanString;


    }
*/

    //check wheter giving string is a letter or harkaat
    private boolean isHarkaat(String text) {

        if (text.equals("ٌ") || text.equals("ً") || text.equals("ٍ"))
            return true;
        if (text.equals("\u0650") || text.equals("\u0651") || text.equals("\u0652") || text.equals("\u0652") || text.equals("\u0653") || text.equals("\u0654") || text.equals("\u0655")
                || text.equals("\u0656") || text.equals("\u0657") || text.equals("\u0658") || text.equals("\u0659") || text.equals("\u065a") || text.equals("\u065b")
                || text.equals("\u065c") || text.equals("\u065d") || text.equals("\u065e") || text.equals("\u065f"))
            return true;

        if (text.equals("\u0610") || text.equals("\u0611") || text.equals("\u0612") || text.equals("\u0612") || text.equals("\u0613") || text.equals("\u0614") || text.equals("\u0615")
                || text.equals("\u0616") || text.equals("\u0617") || text.equals("\u0618") || text.equals("\u0619") || text.equals("\u061a") || text.equals("\u061b")
                || text.equals("\u060c") || text.equals("\u060d") || text.equals("\u061e") || text.equals("\u061f"))
            return true;
        if (text.equals("\u064b") || text.equals("\u064b")
                || text.equals("\u060c") || text.equals("\u060d") || text.equals("\u064e") || text.equals("\u064f"))
            return true;
        if (text.equals("\u06e0") || text.equals("\u06e1") || text.equals("\u06e2") || text.equals("\u06e2") || text.equals("\u06e3") || text.equals("\u06e4") || text.equals("\u06e5")
                || text.equals("\u06e6") || text.equals("\u06e7") || text.equals("\u06e8") || text.equals("\u0671") || text.equals("\u06ea") || text.equals("\u06eb")
                || text.equals("\u06ec") || text.equals("\u06ed") || text.equals("\u0674"))
            return true;
        return text.equals("\u06d7") || text.equals("\u06d8") || text.equals("\u06d9") || text.equals("\u06da") || text.equals("\u06db")
                || text.equals("\u06dc") || text.equals("\u06df");


    }//cls is harkaat


}
