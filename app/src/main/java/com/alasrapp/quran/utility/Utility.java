package com.alasrapp.quran.utility;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by crikle on 1/27/16.
 */
public class Utility {

    public String loadJSONFromAsset(String name, Context c) {
        String json;
        try {

            InputStream is = c.getAssets().open(name + ".json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }//cls load json from assets
}
