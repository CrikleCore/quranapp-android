package com.alasrapp.quran

import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.firebase.analytics.FirebaseAnalytics

import io.fabric.sdk.android.Fabric

/**
 * Created by agilani on 7/1/16.
 */
class Application : android.app.Application() {
    override fun onCreate() {
        super.onCreate()

        FirebaseAnalytics.getInstance(this)
        Fabric.with(this, Crashlytics(), Answers())
    }
}
